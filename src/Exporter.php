<?php

namespace Decrypter;

class Exporter
{
    public static function export($accounts, $format, $destFile)
    {
        $format = strtolower($format);

        switch ($format) 
        {
            case 'keepass':
                self::exportKeepass($accounts, $destFile);
                break;

            case 'lastpass':
                self::exportLastpass($accounts, $destFile);

            default:
                self::exportKeepass($accounts, $destFile);
                break;
        }
    }

    protected static function exportLastpass($accounts, $destFile)
    {
        $csv = fopen($destFile, 'w');
        $header = [
            'url', 'type', 'username', 'password', 'hostname', 'extra', 'name', 'grouping'
        ];

        fputcsv($csv, $header);

        foreach ($accounts as $account) {
            $line = [
                $account['url'],
                '', // Type.
                $account['login'],
                $account['password'],
                '', // Hostname.
                'Customer: ' . $account['customer'],
                $account['name'],
                $account['category']
            ];

            fputcsv($csv, $line);
        }
    }

    protected static function exportKeepass($accounts, $destFile)

    // Keepass CSV Generic import - see https://keepass.info/help/kb/imp_csv.html#exGrp
        
    {
        $csv = fopen($destFile, 'w');
        $header = [
            'Group', 'Account', 'Login name', 'Password', 'Web site', 'Comments'
        ];

        fputcsv($csv, $header);

        foreach ($accounts as $account) {
            $line = [
                $account['category'],
                $account['name'],
                $account['login'],
                $account['password'],
                $account['url'],
                $account['notes']
            ];

            fputcsv($csv, $line);
        }
    }

}