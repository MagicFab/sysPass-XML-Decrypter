SysPass XML decrypter
=====================

XML decrypter for [sysPass](https://github.com/nuxsmin/sysPass/).

Originally by Julien Pardo - https://github.com/julenpardo/sysPass-XML-Decrypter

Tested with PHP 7.0.15 and XML exported with sysPass 2.1.5.17041201. This script has also been reported to work with the latest sysPass v3.

Instructions below are for Debian 10 (Buster).

## Supported export formats

 - Keepass (default) - CSV format suitable for direct import into Keepass
 - LastPass
 
 The resulting file is comma-delimited and may also be suitable for CSV generic import in other applications.

## Requirements

 - PHP >= 7.0
 - php-xml
 - php-mbstring
 
## Installation

 - Clone the repository:

 ```
git clone https://gitlab.com/MagicFab/sysPass-XML-Decrypter.git
```

 - Install Composer dependencies:

 ```
 cd sysPass-XML-Decrypter
 composer install
 ```

## Usage

 - Execute the script:
 ```
./decrypt.php <path-to-encrypted-xml> <xml-export-password> <syspass-master-password> <dest-file> [format]
 ```
 
 [format] is `keepass` or `lastpass`. If unspecified, the default is `keepass`.
 
 **If any of your passwords has spaces, consider using quotes around them.**
