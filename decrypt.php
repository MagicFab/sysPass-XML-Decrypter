#!/usr/bin/env php
<?php

require 'vendor/autoload.php';

define('DEST_FILE', 'decrypted_accounts.txt');

use \Decrypter\Decrypter;
use \Decrypter\Exporter;


function main()
{
    exitIfNotCLI();
    $args = getArguments();

    $decrypter = new Decrypter(
        $args['xmlFile'],
        $args['exportKey'],
        $args['masterKey'],
        $args['format'] // this is either lastpass or keepass, 'default' if none specified
);

    echo "Starting decryption...\n";

    $decryptedAccounts = $decrypter->decrypt();

    echo "Exporting ('" . $args['format'] . "' argument used)...\n";

    Exporter::export($decryptedAccounts, $args['format'], $args['destFile']);

    echo "=============================\n\n";
    echo "Decryption ended with " . count($decryptedAccounts) . " accounts.\n";
}

function exitIfNotCLI()
{
    $sapiName = strtoupper(php_sapi_name());

    if ($sapiName !== 'CLI') {
        echo "This script must be called from the command line.\n";
        exit(1);
    }
}

function getArguments()
{
    global $argc, $argv;

    if ($argc < 5) {
        echo "sysPass XML Decrypter 0.3\nUsage: ./decrypt.php <path-to-encrypted-xml> <xml-export-password> <syspass-master-password> <dest-file> [format]\n\n";
        echo "[format]\n- lastpass: LastPass CSV format (default)\n";
        echo "- keepass: Keepass Generic CSV importer. See 'Example 2 (With Groups)' at https://keepass.info/help/kb/imp_csv.html#exGrp\n";
        exit(2);
    }

    $xmlFile = $argv[1];
    $exportKey = $argv[2];
    $masterKey = $argv[3];
    $destFile = $argv[4];
    $format = $argv[5] ?? '<default>';

    if (!file_exists($xmlFile)) {
        echo "The file '$xmlFile' does not exist or couldn't be read.\n";
        exit(3);
    }

    return [
        'xmlFile' => $xmlFile,
        'exportKey' => $exportKey,
        'masterKey' => $masterKey,
        'destFile' => $destFile,
        'format' => $format
    ];
}

try {
    main();
} catch (Exception $exception) {
    echo "\nSome error happened:\n";
    echo($exception->getMessage()) . "\n\n";
    exit(4);
}